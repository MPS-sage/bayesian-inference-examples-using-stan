functions {
  vector Cauchy (vector x, real  x0, real  g, real I) {
    return I * g^2 ./ ((x-x0).*(x-x0) + g^2);
  }
}
data {
  int<lower=0> N;
  vector[N] x;
  vector[N] y;
}
parameters {
  real<lower=0, upper=200> I;
  real<lower=0, upper=60> x0;
  real<lower=0, upper=60> gamma;
  real<lower=0, upper=10> sigma;
}
model {
  y ~ normal(Cauchy(x, x0, gamma, I), sigma);
}