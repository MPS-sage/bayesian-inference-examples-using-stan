# Lorentizan mixture fitting using Stan
# See https://research-engine.appspot.com/45001/notebooks/6244257530642432/5962782553931776

library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

DO_FIT <- TRUE   # Do the Stan fitting (time-consuming)

set.seed(1234)
x <- seq(1,60, length.out = 1000)
N <- length(x)

# The Lorentzian (Cauchy) distribution is defined as
# f(x) = I * gamma^2 / ((x - x0)^2 + gamma^2)
# I: height
# gamma: half-width at half-maximum
# x0: location parameter
# For f(x) to be a probability density: I=(pi*gamma)^{-1}
Cauchy <- function (x, I, g, x0) {
  return(I * g^2 / ((x - x0)^2 + g^2))
}

# We define the *real* parameters to make some simulated data
I <- c(100,100)
g <- c(1,1)
x0 <- c(20,40)
noise <- 2

# This is the simulated data
y <- rnorm(N, 
           Cauchy(x,I[1],g[1],x0[1]) + Cauchy(x,I[2],g[2],x0[2]),
           noise)
plot(
  xy.coords(x,y),
  xlim=c(0,max(x)), ylim=c(min(y), max(y)),
  xlab="x", ylab="y",
  type="p", pch=16, cex=0.5, col="black")

# We now fit the simulated data and find the mean of the fits
if (DO_FIT){
fit <- stan(file = 'lorentzian_mix_bad.stan', iter = 2000, chains = 4)

posterior <- extract(fit, inc_warmup = FALSE)
mean_I <- vector(length = 2)
mean_g <- vector(length = 2)
mean_x0 <- vector(length = 2)
for (i in 1:2) {
  mean_I[i] <- mean(posterior$I[,i])
  mean_g[i] <- mean(posterior$g[,i])
  mean_x0[i] <- mean(posterior$x0[,i])
}

plot(
  xy.coords(x,y),
  xlim=c(0,max(x)), ylim=c(min(y), max(y)),
  xlab="x", ylab="y",
  type="p", pch=16, cex=0.5, col="black")
lines(x=x,
      y=Cauchy(x,mean_I[1],mean_g[1],mean_x0[1]) 
        + Cauchy(x,mean_I[2],mean_g[2],mean_x0[2]),
      type="l", col="blue", lw="3")
}