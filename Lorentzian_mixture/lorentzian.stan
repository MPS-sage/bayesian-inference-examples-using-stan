functions {
  vector Cauchy (vector x, real  I, real  g, real x0) {
    return I * g^2 ./ ((x-x0).*(x-x0) + g^2);
  }
}
data {
  int<lower=0> N;
  vector[N] x;
  vector[N] y;
}
parameters {
  real<lower=0, upper=max(y)> I;
  real<lower=0, upper=60> x0;
  real<lower=0, upper=60> g;
  real<lower=0, upper=10> noise;
}
model {
    y ~ normal(Cauchy(x, I, g, x0), noise);
}