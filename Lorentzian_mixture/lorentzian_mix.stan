functions {
  vector Cauchy (vector x, real  I, real  g, real x0) {
    return I * g^2 ./ ((x-x0).*(x-x0) + g^2);
  }
}
data {
  int<lower=0> N; // number of observations
  vector[N] x;
  vector[N] y;
  real x_c;
}
parameters {
  real<lower=0, upper=1.2*max(y)> I[2];
  real<lower=min(x), upper=x_c> x01;
  real<lower=x_c, upper=max(x)> x02;
  real<lower=0, upper=60> g[2];
  real<lower=0, upper=10> noise;
}
model {
    y ~ normal(Cauchy(x, I[1], g[1], x01) + Cauchy(x, I[2], g[2], x02),
                      noise);
}