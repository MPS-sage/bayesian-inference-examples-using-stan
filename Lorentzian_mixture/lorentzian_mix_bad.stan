functions {
  vector Cauchy (vector x, real  I, real  g, real x0) {
    return I * g^2 ./ ((x-x0).*(x-x0) + g^2);
  }
}
data {
  int<lower=0> N; // number of observations
  vector[N] x;
  vector[N] y;
}
parameters {
  real<lower=0, upper=200> I[2];
  real<lower=0, upper=60> x0[2];
  real<lower=0, upper=60> g[2];
  real<lower=0, upper=10> noise;
}
model {
  y ~ normal(Cauchy(x, I[1], g[1], x0[1]) + Cauchy(x, I[2], g[2], x0[2]),
                  noise);
}