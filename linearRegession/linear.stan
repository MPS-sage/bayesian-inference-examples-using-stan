data {
  int<lower=0> N;
  vector[N] x;
  vector[N] y;
}
parameters {
  real theta0;
  real theta1;
  real<lower=0> sigma;
}
model {
  y ~ normal(theta0 + theta1 * x, sigma);
}