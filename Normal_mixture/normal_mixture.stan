functions {
  real normal_dist (real x, real m, real s) {
    return exp(-(x-m)^2/(2*s^2))/(s*sqrt(2*pi()));
  }
  real sum_normals (real x, real[] means, real[] sds, real[] heights, int K) {
    real s;
    s <- 0;
    for (i in 1:K)
      s <- s + heights[i] * normal_dist(x, means[i], sds[i]) / normal_dist(0,0,sds[i]);
    return s;
  }
}
data {
  int<lower=0> N;      // number of data points
  int<lower=0> K;      // number of peaks
  vector[N] x;
  vector[N] y;
}
parameters {
  real<lower=0, upper=60> heights[K];
  real<lower=0, upper=60> mus[K];
  real<lower=0, upper=60> sds[K];
  real<lower=0, upper=10> noise;
}
model {
  for (i in 1:N)
    y[i] ~ normal(sum_normals(x[i], mus, sds, heights, K), noise);
}