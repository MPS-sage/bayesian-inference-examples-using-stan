# Simple background simulation for the power density spectra (PSD) of
# a solar-like star (see e.g. doi:10.1051/0004-6361/201424181)
#
# Here we consider the simple example
# P(nu) = W + R(nu)[B_power(nu,a,b)]
#
# W: constant
# R(nu) = sinc(pi*nu/(2*nu*nuNyq))  with nuNyq a constant
# B_power = a*nu**{-b}

library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
RNG_SEED <- 1234
set.seed(RNG_SEED)
source("functions.R")

DO_FIT <- TRUE
nuNyq <- 8496.36   #uHz
nu <- seq(from=80, to=5000, length.out = 5000)
N <- length(nu)

# The *real* parameters
W <- 0.37
a <- 428
b <- 1.13
Hosc <- 0.294
numax <- 1655
sig_env <- 193
noise <- 0.1
tau1 <- 271 / 10^6
sig1 <- 37.2
c1 <- 4.3
tau2 <- 69 / 10^6
sig2 <- 38.4
c2 <- 12.5

bg <- W + (sinc(pi*nu/(2*nuNyq))**2) * B_power(nu, a ,b) + 
          B_excess(nu, Hosc, numax, sig_env) +
          B_Harvey(nu, tau1, sig1, c1) +
          B_Harvey(nu, tau2, sig2, c2) +
          rnorm(nu, mean = 0, sd = noise)

plot(nu, bg, type="l", log="x",
     xlab=expression(paste("Frequency (", mu, "Hz)")), 
     ylab=expression(paste("PSD (ppm"^2, "/", mu, "Hz)")))

if (DO_FIT) {
  fit <- stan(file = '5-terms.stan', iter = 2000, chains = 4, seed = RNG_SEED)
  posterior <- extract(fit, inc_warmup = FALSE)
  
  for (i in 1:500) {
    W_fit <- sample(posterior$W,1)
    a_fit <- sample(posterior$a,1)
    b_fit <- sample(posterior$b,1)
    Hosc_fit <- sample(posterior$Hosc,1)
    numax_fit <- sample(posterior$numax,1)
    sig_env_fit <- sample(posterior$sig_env,1)
    tau1_fit <- sample(posterior$tau1,1)
    sig1_fit <- sample(posterior$sig1,1)
    c1_fit <- sample(posterior$c1,1)
    tau2_fit <- sample(posterior$tau2,1)
    sig2_fit <- sample(posterior$sig2,1)
    c2_fit <- sample(posterior$c2,1)
    noise_fit <- sample(posterior$noise,1)
    lines(nu, W_fit + (sinc(pi*nu/(2*nuNyq))**2) * (B_power(nu, a_fit ,b_fit) +
              B_excess(nu, Hosc_fit, numax_fit, sig_env_fit) +
              B_Harvey(nu, tau1_fit, sig1_fit, c1_fit) +
              B_Harvey(nu, tau2_fit, sig2_fit, c2_fit)),
          type="l", col="blue", lw="0.1")
  }
}


