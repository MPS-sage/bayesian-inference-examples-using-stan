functions {
  vector sinc(vector x, int N) {
    vector[N] res;
    for (i in 1:N)
      res[i] <- sin(x[i])/x[i];
    return res;
  }
  vector B_power(vector x, real a, real b) {
    return a*exp(-b * log(x));
  }
  vector B_excess(vector x, real H, real numax, real sigma) {
    return H * exp(-(x - numax).*(x - numax)/(2 * sigma^2));
  }
  vector B_Harvey(vector nu, real tau, real sig, real c) {
    return 4 * tau * sig^2 ./ (1 + exp(c * log(2 * pi() * nu * tau)));
  }
}
data {
  int<lower=0> N;   # Number of data points
  vector[N] nu;
  vector[N] bg;
  real<lower=0> nuNyq;
}
transformed data {
  vector[N] bg_R;
  bg_R <- sinc((pi()*nu)/(2*nuNyq), N).*sinc((pi()*nu)/(2*nuNyq), N);
}
parameters {
  real<lower=0, upper=max(bg)> W;
  real<lower=0, upper=1000> a;
  real<lower=0, upper=5> b;
  real<lower=0, upper = 1> Hosc;
  real<lower=1000, upper=3000> numax;
  real<lower=0, upper=1000> sig_env;
  real<lower=0, upper=max(bg)> noise;
  real<lower=0, upper=0.001> tau1;
  real<lower=1, upper=100> sig1;
  real<lower=0, upper=10> c1;
  real<lower=0, upper=0.001> tau2;
  real<lower=1, upper=100> sig2;
  real<lower=10, upper=15> c2;
}
model {
  bg ~ normal(W + bg_R .* (B_power(nu,a,b) +
              B_excess(nu, Hosc, numax, sig_env)) +
              B_Harvey(nu, tau1, sig1, c1) +
              B_Harvey(nu, tau2, sig2, c2)
              , noise);
}